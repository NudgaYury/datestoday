﻿using UnityEngine;
using System.Collections;

public class Player : MovingObject {

	private int lives = 3;
	public int Lives 
	{
		get { return lives; }
	}
	private int maxLives = 3;
	public int MaxLives 
	{
		get { return maxLives; }
	}
	private bool isFinished = false;
	public bool IsFinished
	{
		get { return isFinished; }
	}

	public int LivesInc ()
	{
		if (lives < maxLives) lives++;
		return lives;
	}

	public int LivesDec ()
	{
		if (lives > 0) lives--;
		if (lives == 0) isFinished = true;
		return lives;
	}

	private Animator animator;

	private BaseInputController	theUserInput;

	public override void Start () 
	{
		base.Start ();

		isFinished = false;

		animator = GetComponentInChildren <Animator> ();
		base.Start ();

		#if UNITY_EDITOR
			theUserInput = gameObject.AddComponent <KeyboardInput> () as BaseInputController;
//			Debug.Log ("Editooor!!!!");
		#elif UNITY_ANDROID
//			Debug.Log ("Android");
			theUserInput = gameObject.AddComponent <TouchInput> () as BaseInputController;
		#endif

//		moveSpeed = 3.0f;
	}

	// если игрок в начале клетки - определяем направление движения
	public override void ProcessNextDirection ()
	{
		if (nextDirection != Vector2.zero)
		{
			if (level.CanMoveTo ((Vector2)transform.position + nextDirection))
			{
				// игрок может двигаться в следующем направлении 
				moveDirection = nextDirection;
				nextDirection = Vector2.zero;

				return;
			}
		}

		// следующее направление еще не доступно или не определено
		if (!level.CanMoveTo ((Vector2)transform.position + moveDirection))
		{
			// движение невозможно - остановка!
			moveDirection = Vector2.zero;
			nextDirection = Vector2.zero;

			return;
		}

	}

	void Update ()
	{
		int horizontal = 0;
		int vertical = 0;

		horizontal = (int) theUserInput.GetHorizontal ();
		vertical = (int) theUserInput.GetVertical ();

		if (horizontal != 0 || vertical != 0)
		{
			if (moveDirection == Vector2.zero)
			{
				if (horizontal > 0)	moveDirection = Vector2.right;
				if (horizontal < 0)	moveDirection = Vector2.left;
				if (vertical > 0) moveDirection = Vector2.up;
				if (vertical < 0) moveDirection = Vector2.down;
			} else {
				if (horizontal > 0)	
				{
					if (moveDirection == Vector2.left) 	moveDirection = Vector2.right;
					else 								nextDirection = Vector2.right;
				}
				if (horizontal < 0)	
				{
					if (moveDirection == Vector2.right) moveDirection = Vector2.left;
					else 								nextDirection = Vector2.left;
				}
				if (vertical > 0)
				{
					if (moveDirection == Vector2.down) 	moveDirection = Vector2.up;
					else 								nextDirection = Vector2.up;
				}
				if (vertical < 0) 
				{
					if (moveDirection == Vector2.up) 	moveDirection = Vector2.down;
					else 								nextDirection = Vector2.down;
				}
			}
		}

		if (Move ())
		{
			animator.SetInteger ("Horizontal", (int) moveDirection.x);
			animator.SetInteger ("Vertical", (int) moveDirection.y);

			animator.SetBool ("Moving", true);

			if (moveDirection.x > 0) animator.SetBool ("Right", true); else animator.SetBool ("Right", false);
			if (moveDirection.x < 0) animator.SetBool ("Left", true); else animator.SetBool ("Left", false);
			if (moveDirection.y > 0) animator.SetBool ("Up", true); else animator.SetBool ("Up", false);
			if (moveDirection.y < 0) animator.SetBool ("Down", true); else animator.SetBool ("Down", false);
		}
		else
		{
			animator.SetInteger ("Horizontal", 0);
			animator.SetInteger ("Vertical", 0);
			animator.SetBool ("Moving", false);
			animator.SetBool ("Right", false);
			animator.SetBool ("Left", false);
			animator.SetBool ("Up", false);
			animator.SetBool ("Down", false);
		}
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		Debug.Log ("Trigger = " + other);
		if (other.tag == "Rose")
		{
			GameController.Instance.PlayerGetRose (other);
		}

		if (other.tag == "Exit")
		{
			GameController.Instance.LevelComplete ();
		}
	}

}
