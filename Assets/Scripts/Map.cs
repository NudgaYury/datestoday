﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Map {

	public enum CellType
	{
		Wall = 1,
		Space = 0
	}

	public int levelHeight;
	public int levelWidth;

	public List<CellType> map = new List<CellType> ();

	public CellType this [int x, int y]
	{
		get
		{
			return map [y * levelWidth + x];
		}
		set
		{
			map[y*levelWidth + x] = value;
		}
	}

}
