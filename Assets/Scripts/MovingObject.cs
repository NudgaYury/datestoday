﻿using UnityEngine;
using System.Collections;

public class MovingObject : MonoBehaviour {

	public float moveSpeed = 0.5f; // скорость - единицы в секунду (1 unit - 100 pixels - устанавливается в свойствах спрайта)

	public LevelManager level;

	private BoxCollider2D boxCollider;

	public Vector2 moveDirection;
	public Vector2 nextDirection;

	public virtual void Start ()
	{
		boxCollider = GetComponent<BoxCollider2D> ();

		moveDirection = Vector2.zero;
		nextDirection = Vector2.zero;
	}

	public bool IsPreciselyCell ()
	{
		Vector2 position = transform.position;
		if (position.x - Mathf.FloorToInt (position.x) < Mathf.Epsilon &&
			position.y - Mathf.FloorToInt (position.y) < Mathf.Epsilon )
		{
			return true;
		}
		return false;
	}

	// если объект в начале клетки - определяем направление движения
	public virtual void ProcessNextDirection ()
	{
		return;
	}

	public bool Move ()
	{
//		Debug.Log ("----------------------------------------------");
//		Debug.Log ("moveDirection = " + moveDirection.sqrMagnitude );
		if (IsPreciselyCell ())
			ProcessNextDirection ();

		if (moveDirection == Vector2.zero)
			return false;

		Vector2 position = transform.position;

		Vector2 distance = moveDirection * moveSpeed * Time.deltaTime;
		//Debug.Log ("vectorSpeed = " + distance);

		Vector2 newPosition = position + distance;
		//Debug.Log ("newPosition = " + newPosition);

		if (IsPreciselyCell () && (moveDirection == Vector2.left || moveDirection == Vector2.up))
		{
			// если игрок точно в начале клетки и дв-ся влево и вверх - просто двигаемся
			transform.position = newPosition;
			return true;
		}

		// условие перехода на новую клетку
		if (Mathf.Abs ((int)position.x - (int)newPosition.x) > 0 || Mathf.Abs ((int)position.y - (int)newPosition.y) > 0 )
		{
			Vector2 newPositionPreciselyCell = new Vector2();
			// начало новой клетки
			if (moveDirection == Vector2.right || moveDirection == Vector2.up) 
				newPositionPreciselyCell = new Vector2 ((int)newPosition.x, (int)newPosition.y);
			if (moveDirection == Vector2.left || moveDirection == Vector2.down)
				newPositionPreciselyCell = new Vector2 ((int)position.x, (int)position.y);
			
			// расстояние, которое осталось пройти после начала клетки
			Vector2 distanseAfterNewCell = newPosition - newPositionPreciselyCell;

			// устаноавливаем в начало новой клетки
			transform.position = newPositionPreciselyCell;

			// проверяем направление
			ProcessNextDirection ();

			if (moveDirection == Vector2.zero)
				return false;

			// если доступно дв-е далее или другое направление - проходим оставшееся расстояние
			float floatDistanse = Mathf.Abs (distanseAfterNewCell.x + distanseAfterNewCell.y);
			distance = moveDirection * floatDistanse;
			position = transform.position;
			newPosition = position + distance;
			transform.position = newPosition;
			return true;
			
		} else {
			// если не переходим в новую клетку
			transform.position = newPosition;
			return true;
		}

		return false;
	}

}
