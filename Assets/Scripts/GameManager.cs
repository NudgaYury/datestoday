﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	public static GameManager instance = null;
	private LevelManager levelManager;

	// Use this for initialization
	void Awake ()
	{
		if (null == instance)
			instance = this;
		else if (instance != this)
			Destroy (gameObject);

		DontDestroyOnLoad (gameObject);

		levelManager = GetComponent<LevelManager> ();
		InitGame ();
	}

	void InitGame ()
	{
		levelManager.SetupScene ();
	}
	
}
