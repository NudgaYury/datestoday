﻿using UnityEngine;
using System.Collections;

public class TouchInput : BaseInputController {

	private Touch initialTouch = new Touch ();
	private float swipeDistance = 0;
	private bool hasSwiped = false;

	public override void CheckInput ()
	{
		Horizontal = 0;
		Vertical = 0;

		foreach (Touch t in Input.touches)
		{
			if (t.phase == TouchPhase.Began)
			{
				initialTouch = t;
			} else if (t.phase == TouchPhase.Moved && !hasSwiped)
			{
				float deltaX = initialTouch.position.x - t.position.x;
				float deltaY = initialTouch.position.y - t.position.y;
				swipeDistance = Mathf.Sqrt ((deltaX * deltaX) + (deltaY * deltaY));
				bool swipedSideways = Mathf.Abs (deltaX) > Mathf.Abs (deltaY);

				if (swipedSideways && deltaX > 0) // swipe left
				{
					Horizontal = -1;
				} else if (swipedSideways && deltaX <= 0) // swipe right
				{
					Horizontal = 1;
				} else if (!swipedSideways && deltaY > 0) // swipe down
				{
					Vertical = -1;
				} else if (!swipedSideways && deltaY <= 0) // swipe up
				{
					Vertical = 1;
				}

				hasSwiped = true;
			} else if (t.phase == TouchPhase.Ended)
			{
				initialTouch = new Touch ();
				hasSwiped = false;
			}
		}
	}

	public override Vector2 GetDirection ()
	{
		Direction = Vector2.zero;

		if (Horizontal != 0 || Vertical != 0)
		{
			if (Horizontal != 0) Vertical = 0;

			if (Horizontal > 0)	Direction = Vector2.right;
			if (Horizontal < 0)	Direction = Vector2.left;
			if (Vertical > 0) Direction = Vector2.up;
			if (Vertical < 0) Direction = Vector2.down;
		}

		return Direction;
	}

	public void LateUpdate ()
	{
		CheckInput ();
	}

}
