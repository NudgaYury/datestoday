﻿using UnityEngine;
using System.Collections;

public class KeyboardInput : BaseInputController {

	public override Vector2 GetDirection ()
	{
		Direction = Vector2.zero;

		if (Horizontal != 0 || Vertical != 0)
		{
			if (Horizontal != 0) Vertical = 0;

			if (Horizontal > 0)	Direction = Vector2.right;
			if (Horizontal < 0)	Direction = Vector2.left;
			if (Vertical > 0) Direction = Vector2.up;
			if (Vertical < 0) Direction = Vector2.down;
		}

		return Direction;
	}

	public void LateUpdate ()
	{
		CheckInput ();
	}

}
