﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu]
[System.Serializable]
public class LevelData : ScriptableObject {

	public const int CellTypeSpace = 0;
	public const int CellTypeWall = 1;

//	public int levelHeight;
//	public int levelWidth;

	public List<int> map = new List<int> ();

	static int[,] defaultLevelData = new int[11, 20]
	{
		{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
		{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
	};

//	[SerializePrivateVariables]
//	public bool isInit;

	public LevelData ()
	{
//		Debug.Log ("LevelData constructor");
//		Debug.Log (" map = " + map);
//		Debug.Log (" isInit = " + isInit);
//
//		if (isInit) return;
//
//		levelWidth = LevelManager.levelWidth;
//		levelHeight = LevelManager.levelHeight;

		InitDefault ();

	}

	public void OnEnable ()
	{
	}

//	public int this [int x, int y]
//	{
//		get
//		{
//			return map [y * LevelManager.levelWidth + x];
//		}
//		set
//		{
//			map[y * LevelManager.levelWidth + x] = value;
//		}
//	}

	public void Init ()
	{
//		Debug.Log ("Init");

		for (int i =0; i < LevelManager.levelWidth * LevelManager.levelHeight; i++)
			map.Add (0);
		
//		isInit = true;

//		Debug.Log (map.Count);
	}

	public void InitDefault ()
	{
//		Debug.Log ("Init Default");

		for (int y = 0; y < LevelManager.levelHeight; y++)
			for (int x = 0; x < LevelManager.levelWidth; x++)
				map.Add (defaultLevelData [y, x]);

//		isInit = true;

//		Debug.Log (map.Count);
	}

	public void Set (int x, int y, int value)
	{
		if (x >=0 && x < LevelManager.levelWidth && y >=0 && y < LevelManager.levelHeight)
			map [y * LevelManager.levelWidth + x] = value;
	}

	public int Get (int x, int y)
	{
		if (x >=0 && x < LevelManager.levelWidth && y >=0 && y < LevelManager.levelHeight)
			return map [y * LevelManager.levelWidth + x];
		return -1;
	}
}
