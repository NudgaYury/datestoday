﻿using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

public class LevelManager : MonoBehaviour {

	public static int levelWidth = 20;
	public static int levelHeight = 11;

	public int roseCount = 3;

//	public int cellWidth = 100;		// ширина клетки в пикселах
//	public int cellHeight = 100;	// высота клетки в пикселах

//	public enum tileType
//	{
//		nullTile =0,
//		groundTile = 1,
//		wallTile = 2
//	};
//
//	tileType[,] levelData = new tileType[11, 11]
//	{
//		{tileType.wallTile, tileType.wallTile, 		tileType.wallTile, 		tileType.wallTile, tileType.wallTile, tileType.wallTile, tileType.wallTile, tileType.wallTile, tileType.wallTile, tileType.wallTile, tileType.wallTile},
//		{tileType.wallTile, tileType.groundTile, 	tileType.groundTile, 	tileType.groundTile, tileType.groundTile, tileType.groundTile, tileType.groundTile, tileType.groundTile, tileType.groundTile, tileType.groundTile, tileType.wallTile},
//		{tileType.wallTile, tileType.groundTile, 	tileType.wallTile, 		tileType.groundTile, tileType.wallTile, tileType.groundTile, tileType.wallTile, tileType.wallTile, tileType.wallTile, tileType.groundTile, tileType.wallTile},
//		{tileType.wallTile, tileType.groundTile, 	tileType.wallTile, 		tileType.groundTile, tileType.wallTile, tileType.groundTile, tileType.wallTile, tileType.wallTile, tileType.wallTile, tileType.groundTile, tileType.wallTile},
//		{tileType.wallTile, tileType.groundTile, 	tileType.wallTile, 		tileType.groundTile, tileType.wallTile, tileType.groundTile, tileType.groundTile, tileType.groundTile, tileType.wallTile, tileType.groundTile, tileType.wallTile},
//		{tileType.wallTile, tileType.groundTile, 	tileType.groundTile, 	tileType.groundTile, tileType.wallTile, tileType.groundTile, tileType.wallTile, tileType.groundTile, tileType.wallTile, tileType.groundTile, tileType.wallTile},
//		{tileType.wallTile, tileType.groundTile, 	tileType.wallTile, 		tileType.groundTile, tileType.wallTile, tileType.groundTile, tileType.groundTile, tileType.groundTile, tileType.wallTile, tileType.groundTile, tileType.wallTile},
//		{tileType.wallTile, tileType.groundTile, 	tileType.wallTile, 		tileType.groundTile, tileType.wallTile, tileType.wallTile, tileType.wallTile, tileType.wallTile, tileType.wallTile, tileType.groundTile, tileType.wallTile},
//		{tileType.wallTile, tileType.groundTile, 	tileType.wallTile, 		tileType.groundTile, tileType.wallTile, tileType.wallTile, tileType.wallTile, tileType.wallTile, tileType.wallTile, tileType.groundTile, tileType.wallTile},
//		{tileType.wallTile, tileType.groundTile, 	tileType.groundTile, 	tileType.groundTile, tileType.groundTile, tileType.groundTile, tileType.groundTile, tileType.groundTile, tileType.groundTile, tileType.groundTile, tileType.wallTile},
//		{tileType.wallTile, tileType.wallTile, 		tileType.wallTile, 		tileType.wallTile, tileType.wallTile, tileType.wallTile, tileType.wallTile, tileType.wallTile, tileType.wallTile, tileType.wallTile, tileType.wallTile}
//	};

	static int[,] defaultLevelData = new int[11, 20]
	{
		{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
		{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
	};

	public int[,] levelData = new int[11, 20]
	{
		{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1},
		{1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1},
		{1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1},
		{1, 0, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1, 0, 0, 1},
		{1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1},
		{1, 0, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1},
		{1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1},
		{1, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 1, 1, 1, 0, 1},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
		{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
	};

	public LevelData theLevelData;

	public GameObject[] grassTiles;
	public GameObject[] groundTiles;

	public GameObject exit;

	private Transform levelHolder;

	void LevelSetup () 
	{
		if (levelHolder) return;

		levelHolder = new GameObject ("Level").transform;
		levelHolder.transform.SetParent (gameObject.transform);

		for (int y = levelHeight; y >= 0; y--) 
		{
			for (int x = 0; x < levelWidth; x++) 
			{
				GameObject toInstantiate = grassTiles[Random.Range (0, grassTiles.Length)];
				switch (GetTileType(x, y))
				{
				case 0://tileType.groundTile:
					toInstantiate = groundTiles[Random.Range (0, groundTiles.Length)];
					break;
				case 1://tileType.wallTile:
					toInstantiate = grassTiles[Random.Range (0, grassTiles.Length)];
					break;
				}

				GameObject instance = Instantiate (toInstantiate, new Vector3 (x, y, 0.0f), Quaternion.identity) as GameObject;
				instance.transform.SetParent (levelHolder);

				Debug.Log ("loop " + x + ", " + y + " = " + GetTileType (x, y));
			}
		}
	}

	public void SetupScene ()
	{
		LevelSetup ();
	}

	public int GetTileType (int x, int y)
	{
		if (theLevelData == null)
		{
			Debug.Log ("GetTileType theLevelData is NULL");
			return -1;
		}

		return theLevelData.Get (x, y);

//		if (x >= 0 && x < levelWidth && y >= 0 && y < levelHeight) 
//			return theLevelData.map[y * LevelManager.levelWidth + x];
		
//			return levelData [y, x];
		return -1; //tileType.nullTile;
	}

	public void SetTileType (int x, int y, int tileType)
	{
		if (theLevelData == null) 
		{
			Debug.Log ("SetTileType theLevelData is NULL");
			return;
		}

		theLevelData.Set (x, y, tileType);

//		if (x > 0 && x < levelWidth-1 && y > 0 && y < levelHeight-1) 
//			theLevelData.map[y * LevelManager.levelWidth + x] = tileType;

//			levelData [y, x] = tileType;
	}

	public int GetTileType (Vector2 coordinate)
	{
		int x = (int)coordinate.x;
		int y = (int)coordinate.y;
		return GetTileType (x, y);
	}

	public bool CanMoveTo (Vector2 coordinate)
	{
		int x = (int)coordinate.x;
		int y = levelHeight - (int)coordinate.y;
		return /*tileType.groundTile*/ 0 == GetTileType (x, y);
	}

	public void OpenExit ()
	{
		exit.SetActive (true);
		Debug.Log ("exit = " + exit.transform.position + "    " + (int)exit.transform.position.y);
//		levelData [ levelHeight - (int)exit.transform.position.y, (int) exit.transform.position.x] = 0;
		int y = levelHeight - (int)exit.transform.position.y;
		int x = (int) exit.transform.position.x;

//		theLevelData.map[y * LevelManager.levelWidth + x] = 0;
		theLevelData.Set (x, y, 0);
	}

	public void ExitPassed ()
	{
		exit.GetComponentInChildren <Animator> ().SetTrigger ("Passed");
	}
}
