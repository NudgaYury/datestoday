﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CreateAssetMenu]
[System.Serializable]
public class TileSet : ScriptableObject {
	public Transform[] Grounds = new Transform[0];
	public Transform[] Walls = new Transform[0];
	public Transform[] Decorations = new Transform[0];
}
