﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class LevelDataCreateAsset{

//	[MenuItem ("Assets/Create/LevelData")]
	public static void CreateLevelDataAsset ()
	{
		LevelData asset = ScriptableObject.CreateInstance <LevelData> ();
//		asset.Init ();
		asset.InitDefault ();

		AssetDatabase.CreateAsset (asset, "Assets/NewLevelData.asset");
		AssetDatabase.SaveAssets ();
		EditorUtility.FocusProjectWindow ();
		Selection.activeObject = asset;
	}
}
