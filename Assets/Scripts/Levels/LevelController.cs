﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (LevelManager))]
public class LevelController : MonoBehaviour {

	public int sideWidth = 96;
	public Color color = Color.white;

	public bool showGrid = true;
	public bool showPasability = true;

	public LevelManager theLevel;

	public TileSet theTileSet;

	public Transform thePassibilityHolder;
	public SortingLayer sortingLayerPassibility;
	public Transform theDecorationsHolder;
	public SortingLayer sortingLayerDecorations;
	public Transform theObjectsHolder;
	public SortingLayer sortingLayerObjects;


	//public Transform[] rows;

	void Awake ()
	{
		theLevel = gameObject.GetComponent <LevelManager> ();

		thePassibilityHolder = (Transform) this.transform.Find ("PassibilityHolder");
		if (thePassibilityHolder == null) Debug.Log ("!");
		theDecorationsHolder = (Transform) this.transform.Find ("PassibilityHolder");
		if (theDecorationsHolder == null) Debug.Log ("!!");
		theObjectsHolder = (Transform) this.transform.Find ("PassibilityHolder");
		if (theObjectsHolder == null) Debug.Log ("!!!");
	}

	void OnEnable ()
	{
		if (showPasability) 
			InitGround ();
	}

	void OnDrawGizmos ()
	{
		if (showGrid)
		{
			Gizmos.color = color;

			for (int y = 0; y <= LevelManager.levelHeight ; y++)
			{
				Gizmos.DrawLine (new Vector3 (0f, y, 0f), new Vector3 (LevelManager.levelWidth, y, 0f));
			}
			for (int x = 0; x < LevelManager.levelWidth + 1; x++)
			{
				Gizmos.DrawLine (new Vector3 (x, 0, 0f), new Vector3 (x, LevelManager.levelHeight, 0f));
			}
		}

		if (showPasability)
		{
			for (int y = 0; y < LevelManager.levelHeight; y++) 
			{
				for (int x = 0; x < LevelManager.levelWidth; x++) 
				{
					switch (theLevel.GetTileType (x, y))
					{
					case 0://tileType.groundTile:
						Gizmos.color = new Color (0, 1, 0, 0.4f);
						break;
					case 1://tileType.wallTile:
						Gizmos.color = new Color (1, 0, 0, 0.4f);
						Gizmos.DrawCube (new Vector3 (x + 0.5f, LevelManager.levelHeight - y - 0.5f, 0f), new Vector3 (1, 1, 1));
						break;
					}
				}
			}
		}
	}

	void InitGround ()
	{
		if (!thePassibilityHolder) return;
		if (!theTileSet) return;
		if (!theLevel) return;

		for (int y = 0; y < LevelManager.levelHeight; y++) 
		{
			for (int x = 0; x < LevelManager.levelWidth; x++) 
			{
				GameObject toInstantiate = theTileSet.Walls[Random.Range (0, theTileSet.Walls.Length)].gameObject;
				switch (theLevel.GetTileType (x, y))
				{
				case 0://tileType.groundTile:
					toInstantiate = theTileSet.Grounds[Random.Range (0, theTileSet.Grounds.Length)].gameObject;
					break;
				case 1://tileType.wallTile:
					toInstantiate = theTileSet.Walls[Random.Range (0, theTileSet.Walls.Length)].gameObject;
					break;
				}

				GameObject instance = Instantiate (toInstantiate, new Vector3 (x, LevelManager.levelHeight - y, 0.0f), Quaternion.identity) as GameObject;
				instance.transform.SetParent (thePassibilityHolder);
			}
		}
	}

	void ClearAllLayers ()
	{
//		int objectCounts = thePassibilityHolder.childCount;
//		GameObject objects = thePassibilityHolder.GetChild
//		foreach (GameObject obj 
	}

	void Update ()
	{
	}
}
