﻿using UnityEngine;
using System.Collections;

public class BaseGameController : MonoBehaviour {

	bool paused = false;
	public bool Paused 
	{
		get { return paused; }
		set { paused = value; if (paused) Time.timeScale = 0f; else Time.timeScale = 1f; }
	}

	public virtual void StartGame ()
	{
	}

	public virtual void RestartGameButtonPressed ()
	{
		Application.LoadLevel (Application.loadedLevelName);
	}

}
