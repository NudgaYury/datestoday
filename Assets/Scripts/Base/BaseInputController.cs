﻿using UnityEngine;
using System.Collections;

public class BaseInputController : MonoBehaviour {

	public bool Up = false;
	public bool Down = false;
	public bool Right = false;
	public bool Left = false;

	public float Vertical = 0;
	public float Horizontal = 0;

	public Vector2 Direction = new Vector2 (0, 0);

	public virtual void CheckInput ()
	{
		Vertical = Input.GetAxisRaw ("Vertical");
		Horizontal = Input.GetAxisRaw ("Horizontal");
	}

	public virtual Vector2 GetDirection ()
	{
		Direction = Vector2.zero;

		Direction.x = Horizontal;
		Direction.y = Vertical;

		return Direction;
	}

	public virtual float GetHorizontal ()
	{
		return Horizontal;
	}

	public virtual float GetVertical ()
	{
		return Vertical;
	}

}
