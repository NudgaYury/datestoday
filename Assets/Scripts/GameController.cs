﻿using UnityEngine;
using System.Collections;

public class GameController : BaseGameController {

	private static GameController instance;

/*	public GameController () 
	{
		if (instance != null)
		{
			Debug.LogError ("Cannot have 2 instances of GameController!");
			return;
		}

		instance = this;
	}*/

	public static GameController Instance
	{
		get { return instance; }
	}

	void Awake ()
	{
		if (null == instance)
			instance = this;
		else if (instance != this)
			Destroy (gameObject);

		DontDestroyOnLoad (gameObject);

	}

	public LevelManager theLevelManager;
	public Player thePlayer;

	public int rosePicked;

	private bool didInit = false;

	public void OnLevelWasLoaded () 
	{
		Debug.Log ("OnLevelWasLoaded");
		if (!didInit) Init ();
	}

	public void Start () 
	{
		Debug.Log ("Start");
		if (!didInit) Init ();
	}

	public void Init ()
	{
		Debug.Log ("Init");
//		Debug.Log (GameObject.Find ("LevelManager"));
//		Debug.Log (GameObject.Find ("Player"));
		theLevelManager = GameObject.Find ("LevelManager").GetComponent <LevelManager> ();
		thePlayer = GameObject.Find ("Player").GetComponent <Player> ();
		 
		// load level?
		if (theLevelManager == null)
			Debug.LogError ("No LevelManager founded! Drag LevelManager object to script variable!");

		if (thePlayer == null)
			Debug.LogError ("No Player founded! Drag LevelManager object to script variable!");
		
		theLevelManager.SetupScene ();
		thePlayer.level = theLevelManager;

		rosePicked = 0;

		didInit = true;
	}

	public void Update ()
	{
		if (Input.GetKeyDown ("r")) RestartGameButtonPressed ();
		if (Input.GetKeyDown ("p")) Paused = !Paused;
	}

	public void PlayerGetRose(Collider2D rose)
	{
		Debug.Log ("Get rose!!!");
		Destroy (rose.gameObject);
		rosePicked++;

		if (rosePicked == theLevelManager.roseCount) OpenExit ();
	}

	void OpenExit ()
	{
		Debug.Log ("Open exit!");
		theLevelManager.OpenExit ();
	}

	public void LevelComplete ()
	{
		Debug.Log ("Level complete!");
		theLevelManager.ExitPassed ();
		Invoke ("RestartGameButtonPressed", 1f);

		didInit = false;
	}
}
