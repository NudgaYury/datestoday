﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor (typeof (LevelData))]
public class LevelDataEditor : Editor {

	private SerializedObject theLevelData;
	private SerializedProperty theMap;
//	private SerializedProperty levelWidth;
//	private SerializedProperty levelHeight;


	void OnEnable ()
	{
		theLevelData = new SerializedObject (target);
		theMap = theLevelData.FindProperty ("map");
//		levelWidth = theLevelData.FindProperty ("levelWidth");
//		levelHeight = theLevelData.FindProperty ("levelHeight");
	}

	public override void OnInspectorGUI ()
	{
//		base.OnInspectorGUI ();

		if (theMap.arraySize < LevelManager.levelWidth * LevelManager.levelHeight)
		{
			Debug.Log ("InspectorGUI: Map.count = " + theMap.arraySize);
			return;
		}

		theLevelData.Update ();
//		serializedObject.Update ();

		GUILayout.Label ("Level Data");

//		EditorGUILayout.PropertyField (levelWidth);
//		EditorGUILayout.PropertyField (levelHeight);

		for (int y = 0; y < LevelManager.levelHeight; y++)
		{
			GUILayout.BeginHorizontal ();
			for (int x = 0; x < LevelManager.levelWidth; x++)
			{
				SerializedProperty tile = theMap.GetArrayElementAtIndex (y * LevelManager.levelWidth + x);
				string text = " ";
				int tileValue = tile.intValue;
				if (tileValue == LevelData.CellTypeWall) text = "X";
				if (GUILayout.Button (text, GUILayout.Width (20)))
				{
					if (tileValue == LevelData.CellTypeSpace)
						tile.intValue = LevelData.CellTypeWall;
					if (tileValue == LevelData.CellTypeWall)
						tile.intValue = LevelData.CellTypeSpace;
//					Debug.Log (tile);
				}
			}
			GUILayout.EndHorizontal ();
		}

		theLevelData.ApplyModifiedProperties ();
//		serializedObject.ApplyModifiedProperties ();
			

//		for (int y = 0; y < LevelData.LEVEL_HEIGHT; y++)
//		{
//			GUILayout.BeginHorizontal ();
//			for (int x = 0; x < LevelData.LEVEL_WIDTH; x++)
//			{
//				string text = theLevelData.levelPasabilityData [LevelData.LEVEL_HEIGHT - y, x] == 0 ? " " : "X";
//				GUILayout.Button (text, GUILayout.Width (20));
////				if (GUILayout.Button (text, GUILayout.Width (20)))
////				{
////					if (theLevelData.levelPasabilityData [LevelData.LEVEL_HEIGHT - y, x] == 0)
////					{
////						theLevelData.levelPasabilityData [LevelData.LEVEL_HEIGHT - y, x] = 1;
////					}else{
////						theLevelData.levelPasabilityData [LevelData.LEVEL_HEIGHT - y, x] = 0;
////					}
////				}
//			}
//			GUILayout.EndHorizontal ();
//		}

	}

}

//[CustomPropertyDrawer (typeof (LevelData))]
//public class LevelDataDrawer : PropertyDrawer
//{
//	const int propertyHeight = 300;
//
//	public override float GetPropertyHeight (SerializedProperty prop, GUIContent label)
//	{
//		return propertyHeight;
//	}
//
//	public override void OnGUI (Rect position, SerializedProperty prop, GUIContent label)
//	{
//		base.OnGUI (position, prop, label);
//		GUILayout.Label ("Test");
//	}
//}




