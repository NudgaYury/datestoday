﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(LevelManager))]
public class LevelGridEditor : Editor {

	private bool isEnabled;
	private LevelManager theLevel;

	private SerializedObject theLevelObject;
	private SerializedObject theLevelDataObject;
	private SerializedProperty theLevelData;

//	private LevelManager theLevelManager;

//	public Transform thePassibilityHolder;
//	public Transform theDecorationsHolder;
//	public Transform theObjectsHolder;

	void OnEnable () 
	{
		theLevelObject = new SerializedObject (target);
//		Debug.Log (" Level = " + theLevel);
//		Debug.Log (" property = " + theLevelData + theLevelData.arraySize);
		theLevel = (LevelManager)target;
		isEnabled = true;

		theLevelData = theLevelObject.FindProperty ("theLevelData");
		Debug.Log (" theLevelData = " + theLevelData.name);

//		thePassibilityHolder = theLevelController.transform.FindChild ("PassibilityHolder");
//		theDecorationsHolder = theLevelController.transform.FindChild ("DecorationsHolder");
//		theObjectsHolder = theLevelController.transform.FindChild ("ObjectsHolder");
	}

	void OnDisabled ()
	{
		isEnabled = false;
	}

	public override void OnInspectorGUI ()
	{
		base.OnInspectorGUI ();

//		thePassibilityHolder = (Transform) EditorGUILayout.ObjectField ("Passibility Layer", thePassibilityHolder, typeof (Transform), true);
//		if (!thePassibilityHolder)
//			EditorGUILayout.HelpBox ("Passibility Layer is missing!", MessageType.Error);
//		theDecorationsHolder = (Transform) EditorGUILayout.ObjectField ("Decorations Layer", theDecorationsHolder, typeof (Transform), true);
//		if (!theDecorationsHolder)
//			EditorGUILayout.HelpBox ("Decorations Layer is missing!", MessageType.Error);
//		theObjectsHolder = (Transform) EditorGUILayout.ObjectField ("Objects Layer", theObjectsHolder, typeof (Transform), true);
//		if (!theObjectsHolder)
//			EditorGUILayout.HelpBox ("Objects Layer is missing!", MessageType.Error);
//
//		GUILayout.BeginHorizontal ();
//		GUILayout.Label ("Grid size");
//		theLevelController.sideWidth = (int)EditorGUILayout.Slider (theLevelController.sideWidth, 1, 100, null);
//		GUILayout.EndHorizontal ();



//		if (!theLevelManager)
//		{
//			if (GUILayout.Button ("Create Level"))
//			{
//				theLevelManager = theLevelController.gameObject.AddComponent <LevelManager> ();
//			}
//		}else{
//			if (GUILayout.Button ("Reset Level"))
//			{
//			}
//		}

//		theLevelController.theTileSet = (TileSet)EditorGUILayout.ObjectField ("Tile Set", theLevelController.theTileSet, typeof (TileSet), false);

		theLevelObject.Update ();
//		theLevelData.serializedObject.Update ();

		GUILayout.Label ("Level Data");
//		width = (int)EditorGUILayout.Slider (width, 1, 100, null);
//		GUILayout.BeginHorizontal ();
//		for (int y = 0; y < width; y++)
//		{
//			GUILayout.Button ("X", GUILayout.Width (20));
//		}
//		GUILayout.EndHorizontal ();

		if (theLevel.theLevelData == null) return;

		for (int y = 0; y < LevelManager.levelHeight; y++) 
		{
			GUILayout.BeginHorizontal ();
			for (int x = 0; x < LevelManager.levelWidth; x++)
			{
				string text = theLevel.GetTileType (x, y) == 0 ? " " : "X";
				if (GUILayout.Button (text, GUILayout.Width (20)) )
				{
					if (text == "X")
					{
						theLevel.SetTileType (x, y, 0);
					}else{
						theLevel.SetTileType (x, y, 1);
					}
					if (SceneView.sceneViews.Count > 0)
					{
						SceneView sceneView = (SceneView)SceneView.sceneViews[0];
						sceneView.Focus();
					}
				}
			}
			GUILayout.EndHorizontal ();
		}

		theLevelObject.ApplyModifiedProperties ();


		EditorGUILayout.Separator ();

		theLevelDataObject = new SerializedObject (theLevelObject.FindProperty ("theLevelData").objectReferenceValue);
		theLevelDataObject.Update ();
		for (int y = 0; y < LevelManager.levelHeight; y++) 
		{
			GUILayout.BeginHorizontal ();
			for (int x = 0; x < LevelManager.levelWidth; x++)
			{
				SerializedProperty tile = theLevelDataObject.FindProperty ("map").GetArrayElementAtIndex (y*LevelManager.levelWidth + x);
				string text = (tile.intValue == 0) ? " " : "X";
				if (GUILayout.Button (text, GUILayout.Width (20)) )
				{
					if (text == "X")
					{
						tile.intValue = 0;
					}else{
						tile.intValue = 1;
					}
				}
			}
			GUILayout.EndHorizontal ();
		}
		theLevelDataObject.ApplyModifiedProperties ();
//
//		theLevelData.serializedObject.ApplyModifiedProperties ();
	}

//	void OnSceneGUI ()
//	{
//		int controlID = GUIUtility.GetControlID (FocusType.Passive);
//		Event e = Event.current;
//
//		if (e.isMouse && e.type == EventType.mouseDown)
//		{
//			GUIUtility.hotControl = controlID;
//			e.Use ();
//			Ray ray = HandleUtility.GUIPointToWorldRay (e.mousePosition);
//			Debug.Log(ray + "      " + ray.origin);
//			Vector3 mousePosition = ray.origin;
//
//			GameObject newGameObject;
//			Transform prefabNewObject = theLevelController.theTileSet.Grounds[0];
//
//			if(prefabNewObject)
//			{
//				newGameObject = (GameObject) PrefabUtility.InstantiatePrefab (prefabNewObject.gameObject);
//				Vector3 aligned = new Vector3 (Mathf.Floor (mousePosition.x), Mathf.Floor (mousePosition.y), 0);
//				newGameObject.transform.position = aligned;
//				newGameObject.transform.SetParent (theLevelController.transform);
//			}
//		}
//
//		if (e.isMouse && e.type == EventType.MouseUp)
//		{
//			GUIUtility.hotControl = 0;
//		}
//	}

	void GridUpdate (SceneView sceneView)
	{
		Event e = Event.current;
	}
}
